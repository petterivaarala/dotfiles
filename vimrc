set nocompatible               " be iMproved
filetype off                   " required!

" Vundle BEGIN
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle, required!
Bundle 'gmarik/vundle'

" My Bundles here:
"
"Bundle 'airblade/vim-gitgutter'
Bundle 'tpope/vim-fugitive'
Bundle 'bling/vim-airline'
set laststatus=2

filetype plugin indent on     " required!
"
" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install(update) bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle command are not allowed..

" Vundle END


set t_Co=256
syntax enable
set background=dark
colorscheme molokai

" Show line numbers on the side in grey, and also show on the bar
set ruler                     " show the line number on the bar
set number                    " line numbers
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE

set autoread                  " watch for file changes
set autoindent smartindent    " auto/smart indent
set smarttab                  " tab and backspace are smart

set scrolloff=3               " keep at least 5 lines above/below

set backspace=indent,eol,start

" Wrap long line, add <> when wrapping, also show whitespace at the end of lines
set nowrap
set list listchars=tab:>-,trail:.,precedes:<,extends:>

" Use spaces instead of tabs
set tabstop=4
set shiftwidth=4
set expandtab

" Search related
set hlsearch
set ignorecase
set incsearch
" clear search highlight
cab nh noh

" Open files in tabs
cab t tabe

